import React from 'react';
import { Input, Button, Select, Modal, message, Row } from 'antd';


import Form, { FormProps } from 'antd/lib/form';
import moment from 'moment';

import './style.less';
import { QueryUserPage } from '../../Service/model';
import { queryUserPage } from '../../Service/UserService';

const confirm = Modal.confirm;

const FormItem = Form.Item;
const Option = Select.Option;

const appMap = new Map<string, string>().set("101", "微信小程序").set("201", "安卓商户版APP").set("202", "IOS商户版APP");

type UserSearchFormProps = {
  // setAlitaState: (param: any) => void;
  onSubmit: (param: QueryUserPage) => void;
}


interface UserSearchFormState extends QueryUserPage {

}

const initState: Partial<UserSearchFormState> = {
  loginName: undefined,
}

class UserSearchForm extends React.Component<UserSearchFormProps, UserSearchFormState> {

  constructor(props: any) {
    super(props);
    this.state = initState
  }

  componentDidMount() {

  };

  resetForm() {
    this.setState({
      loginName: undefined,
    });
  }

  submit() {
    const queryParams: QueryUserPage = {
      ...this.state
    }
    this.props.onSubmit(queryParams);
  };

  onNameChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (event && event.target) {
      const loginName = event.target.value;
      this.setState({
        loginName
      });
    }
  };


  render() {

    const { loginName } = this.state;

    return (
      <div>
        <Form layout="inline">
          <FormItem label="用户账号">
            <Input
              placeholder="输入用户账号搜索" value={loginName} onChange={this.onNameChange}
            />
          </FormItem>

          <br />

          <Row className="searchAction">
            <Button type="primary" onClick={() => this.submit()} >
              搜索
              </Button>
            <Button onClick={() => this.resetForm()}>
              重置
            </Button>
          </Row>
        </Form>
      </div >

    );
  }
}

export default UserSearchForm;
