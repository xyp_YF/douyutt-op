import React, { Fragment } from 'react';
import { Table, Divider, Card, Input, Icon, Button, Select, Modal, message, Row } from 'antd';
import { ColumnProps, PaginationConfig } from 'antd/lib/table';
import { Link } from 'react-router-dom';

import Form from 'antd/lib/form';

import moment from 'moment';
import { UserVO, QueryUserPage, PageInfo } from '../Service/model';
import { queryUserPage, deleteUser } from '../Service/UserService';
import UserSearchForm from './component/UserSearchForm';
import { CirclePicker, ColorResult } from 'react-color';

const confirm = Modal.confirm;

const FormItem = Form.Item;
const Option = Select.Option;

interface UserPageState {
  list: any[];
  page: PageInfo;
}

class UserPage extends React.Component<{}, UserPageState> {
  state: UserPageState = {
    // list: [{}],
    list: [],
    page: {
      pageSize: 20,
      currentPage: 1,
    },
  };

  componentDidMount() {
    this.initData();
  }

  columns: ColumnProps<UserVO>[] = [
    {
      title: '编号',
      dataIndex: 'id',
    }, {
      title: '用户名',
      dataIndex: 'loginName'
    },
    /**<Button type="primary" shape="circle"> */
    {
      title: '最后登录时间',
      dataIndex: 'updatedTime',
      render: (text: any, record: UserVO) => {
        const time = moment(record.updatedTime).format("YYYY-MM-DD HH:mm:ss");
        return <span>{time}</span>;
      },
    },
    {
      title: '最后登录IP',
      dataIndex: 'registerIp',
    },
    {
      title: '创建时间',
      dataIndex: 'createdTime',
      render: (text: any, record: UserVO) => {
        const time = moment(record.createdTime).format("YYYY-MM-DD HH:mm:ss");
        return <span>{time}</span>;
      },
    },
    // {
    //   title: '用户状态',
    //   dataIndex: 'status',
    // },
    {
      title: '操作',
      fixed: 'right',
      width: 200,
      render: (_text: any, record: UserVO) => (
        <Fragment >
          <Button size="small">
            <Link to={`/app/user/userForm?id=${record.id}`}>编辑</Link>
          </Button>
          <Divider type="vertical" />
          <Button size="small" type="danger" onClick={() => this.delete(record.id)}>删除</Button>

        </Fragment>
      ),
    },
  ];

  handleTableChange = (pagination: Partial<PaginationConfig>) => {
    const { page } = this.state;
    console.log('currentpage', pagination.current);
    this.onSearchFormSubmit({ pageInfo: { ...page, currentPage: pagination.current! } });
  };

  delete = (id: number) => {
    const _this = this;
    confirm({
      title: '提示',
      content: "确定删除该用户？",
      onOk() {
        console.log('OK');
        deleteUser(id).then(res => {
          if (res.statusCode === '200') {
            message.success("删除用户成功！");
            _this.onSearchFormSubmit({});
          } else {
            message.error("删除用户失败！");
          }
        });
      },
      onCancel() {
        console.log('Cancel');
      },
    });
  }


  initData() {
    this.onSearchFormSubmit({});
  }

  onSearchFormSubmit = (queryParams: Partial<QueryUserPage>) => {
    console.log('onSearchFormSubmit', queryParams);

    queryUserPage({ ...queryParams }).then((res) => {
      if (res.resultCode === '200' && res.result) {
        this.setState({
          list: res.result.list ? res.result.list : [],
          page: res.result.page,
        });
      }
    });
  };


  keyGenerator = (vo: UserVO) => `${vo.id}`;
  render() {
    const {
      list,
      page,
    } = this.state;

    const pagination: PaginationConfig = {
      current: page.currentPage,
      total: page.totalCount,
      pageSize: page.pageSize,
    };

    return (
      <div>
        <Card>
          <UserSearchForm onSubmit={this.onSearchFormSubmit} />
        </Card>
        <br />
        <Card>
          <Row style={{ margin: "10px" }}>
            <Button style={{ float: "right" }} type="primary">
              <Link to={`/app/user/userForm`}>新增用户</Link>
            </Button>
          </Row>
          <Table
            rowKey={this.keyGenerator}
            columns={this.columns}
            pagination={pagination}
            dataSource={list}
            onChange={this.handleTableChange}
          />
        </Card>
      </div>
    );
  }
}

export default UserPage;
