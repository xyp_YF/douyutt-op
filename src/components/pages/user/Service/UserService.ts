import { zbPost, zbGet } from '../../../../axios/tools';
import { QueryUserPage, UserVO } from './model';

export const queryUserPage = (params: Partial<QueryUserPage>) =>
    zbPost({
        url: '/admin/user/queryUserPage',
        data: params,
        msg: '',
    });

export const getUserDetailsById = (id: number) =>
    zbGet({
        url: '/admin/user/getUserDetailsById/' + id,
        msg: '',
    });

    
export const saveUser = (params: Partial<UserVO>) =>
    zbPost({
        url: '/admin/user/updateUser',
        data: params,
        msg: '',
    });

export const register = (params: Partial<UserVO>) =>
    zbPost({
        url: '/user/register',
        data: params,
        msg: '',
    });

export const deleteUser = (id: number) =>
    zbPost({
        url: '/admin/user/delete',
        data: { id },
        msg: '',
    });

