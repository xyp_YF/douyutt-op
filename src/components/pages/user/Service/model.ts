export interface UserVO {
  loginName:string;
  mobile:string;
  createdTime:string;
  updatedTime:string;
  registerIp:string;
  passwd:string;
  id:number;
}

export interface QueryUserPage {
    loginName?: string;
    pageInfo?: PageInfo;
}

export interface PageInfo {
    pageSize: number;
    currentPage: number;
    pageCount?: number;
    totalCount?: number;
}


