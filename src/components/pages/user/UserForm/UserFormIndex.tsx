import React, { FormEvent } from 'react';
import { Button, Card, DatePicker, Input, message, Modal, Row, Select } from 'antd';

import Form, { FormProps } from 'antd/lib/form';
import './style.less';
import { RouteComponentProps } from 'react-router-dom';
import { queryString } from '../../../../utils';
import { UserVO } from '../Service/model';
import { getUserDetailsById, register, saveUser } from '../Service/UserService';

// const { RangePicker } = DatePicker;
// const dateFormat = 'YYYY/MM/DD';
// const confirm = Modal.confirm;

// const FormItem = Form.Item;
// const Option = Select.Option;


type UserFormProps = {} & FormProps & RouteComponentProps;

interface UserFormState extends Partial<UserVO> {
  //   id?: number;
  //   passwd2?: string;
  //   allProjectList?: Array<ProjectVO>;
  //   userProjectList?: Array<ProjectVO>;
  //   userProjectIds?: number[];
}

// const eProjectList = new Array<ProjectVO>();

// const initState: Partial<UserFormState> = {
//   passwd2: undefined,
//   allProjectList: eProjectList,
//   userProjectList: eProjectList,
//   userProjectIds: []
// }

class UserFormIndex extends React.Component<UserFormProps, UserFormState> {
  //   constructor(props: any) {
  //     super(props);
  //     this.state = initState;
  //   }

  //   componentDidMount() {
  //     var param = queryString();
  //     const id = param.id;
  //     this.setState({ id });
  //     this.initForm(id);
  //   }

  //   initForm(id: number) {
  //     let values: Partial<UserVO> = {};
  //     id && getUserDetailsById(id!).then(res => {
  //       if (res.result) {
  //         values = res.result;
  //         this.setState({
  //           ...values
  //         })
  //         this.renderForm(values);
  //       }
  //     });




  //     this.renderForm(values);
  //   }


  //   // updateUserProjectIds(userProjectList: ProjectVO[]) {
  //   //   const userProjectIds: number[] = [];
  //   //   userProjectList.map(vo => {
  //   //     userProjectIds.push(vo.id);
  //   //   })
  //   //   this.setState({
  //   //     userProjectIds
  //   //   })
  //   // }
  //   renderForm(fieldsValue: Partial<UserFormState>) {
  //     // let color :ColorResult ={
  //     //   hex: fieldsValue.tagContent,
  //     // hsl: HSLColor;
  //     // rgb: RGBColor;
  //     // }

  //     // this.setState({ tagContent: fieldsValue.tagContent })
  //     // this.setState({ color: (JSON.parse(fieldsValue.tagContent) as ColorResult).rgb })
  //     // this.setState({imgUrl:})
  //     this.props.form!.setFieldsValue(fieldsValue, () => {

  //     });
  //   }

  //   formItemLayout = {
  //     labelCol: {
  //       xs: { span: 24 },
  //       sm: { span: 8 },
  //     },
  //     wrapperCol: {
  //       xs: { span: 24 },
  //       sm: { span: 14 },
  //     },
  //   };



  //   handleSubmit = (e: FormEvent) => {
  //     e.preventDefault();
  //     const { id } = this.state;
  //     this.props.form!.validateFields((err, values) => {
  //       if (!err) {
  //         console.log("handleSubmit", values);
  //         if (values.passwd && values.passwd2) {
  //           if (values.passwd != values.passwd2) {
  //             message.error("请确认两次输入的密码一致！")
  //             return;
  //           }
  //         }


  //         const data: UserVO = {
  //           ...values,
  //           id,
  //         }
  //         if (data.id) {
  //           // this.updateUser(data);
  //         } else {
  //           // this.createUser(data);
  //         }

  //       };
  //     });
  //   };

  //   // updateUserProject(userId: number) {
  //   //   const { userProjectIds } = this.state;
  //   //   saveUserProject(userId, userProjectIds!).then(res => {
  //   //     if (res.statusCode === '200') {
  //   //       message.success("用户项目授权成功！");
  //   //     }
  //   //   })
  //   // }


  //   // createUser(vo: Partial<UserVO>) {
  //   //   register(vo).then(res => {
  //   //     if (res.statusCode === '200') {
  //   //       this.updateUserProject((res.result.user as UserVO).id);
  //   //       this.props.history.goBack();
  //   //       message.success("创建成功");
  //   //     }
  //   //   })
  //   // }

  //   // updateUser(vo: Partial<UserVO>) {
  //   //   saveUser(vo).then(res => {
  //   //     if (res.statusCode === '200') {
  //   //       this.updateUserProject(vo.id!);
  //   //       this.props.history.goBack();
  //   //       message.success("更新成功");
  //   //     }
  //   //   })
  //   // }


  //   handleChange(value: number[]) {
  //     this.setState({
  //       userProjectIds: value
  //     })
  //     console.log(`selected ${value}`);
  //   }

  render() {
    //     const { form } = this.props;

    //     let {
    //       // appCode,
    //       loginName, passwd, id, userProjectList, allProjectList, userProjectIds
    //     } = this.state;
    //     const { getFieldDecorator } = form!;
    //     // userProjectIds = [];
    //     // userProjectIds.push(1);
    //     // allProjectList!.map(vo => {
    //     //   children.push(<Option key={vo.id.toString()}>{vo.name}</Option>);
    //     // })
    //     console.log("render:", this.state, userProjectIds)
    return (<div />);
    //     return (
    //       <div>

    //         <Card title="用户账户">
    //           <Form onSubmit={this.handleSubmit}>
    //             <FormItem {...this.formItemLayout} label="用户账户">
    //               {
    //                 id ? <span>{loginName}</span> :
    //                   getFieldDecorator('loginName', {
    //                     rules: [{ required: true, message: '请输入登录账号!' }],
    //                   })(<Input placeholder="请输入登录账号" />)
    //               }
    //             </FormItem>

    //             <FormItem {...this.formItemLayout} label="手机号">
    //               {getFieldDecorator('mobile', {
    //                 rules: [{ required: true, message: '请输入手机号!' }],
    //               })(<Input placeholder="请输入手机号" />)}
    //             </FormItem>



    //             <FormItem {...this.formItemLayout} label="新密码">
    //               {getFieldDecorator('passwd', {
    //                 rules: [{ message: '请输入密码!' }],
    //               })(<Input.Password style={{ width: "300px" }} placeholder="请输入密码" />)}
    //             </FormItem>

    //             <FormItem {...this.formItemLayout} label="确认密码">
    //               {getFieldDecorator('passwd2', {
    //                 rules: [{ message: '请输入密码!' }],
    //               })(<Input.Password style={{ width: "300px" }} placeholder="请输入密码" />)}
    //             </FormItem>



    //             <Row className="submitAction">
    //               <Button type="primary" htmlType="submit">提交</Button>
    //               <Button onClick={() => {
    //                 this.props.history.goBack();
    //               }}
    //               >取消</Button>
    //             </Row>
    //           </Form>
    //         </Card>
    // </div >

  }
}

export default Form.create()(UserFormIndex);
