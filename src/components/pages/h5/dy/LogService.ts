import { zbGet, zbPost } from '../../../../axios/tools';
import { LSUser } from './DouyuPage';

export const getLSUser = (name:any) =>
    zbPost({
        url: '/douyu/getLsUser',
        data: {name},
        msg: '',
    });


  export const queryLSUserPage = () =>
    zbPost({
        url: '/douyu/getAll',
        data: {},
        msg: '',
    });


export const updateUser = (user:Partial<LSUser>) =>
    zbPost({
        url: '/douyu/updateUser',
        data: user,
        msg: '',
    });

    export const delUser = (user:Partial<LSUser>) =>
    zbPost({
        url: '/douyu/delUser',
        data: user,
        msg: '',
    });

    export const send = () =>
    zbPost({
        url: '/douyu/send',
        data: {},
        msg: '',
    });

    export const send2 = () =>
    zbGet({
        url: '/douyu/send',
        data: {},
        msg: '',
    });