import React, { Fragment } from 'react';
import { Button, Card, Col, Divider, Icon, Input, List, message, Modal, Row, Skeleton, Table, Tooltip } from 'antd';
import './style.less';

import moment from 'moment';

import { queryString } from '../../../../utils';
import umbrella from 'umbrella-storage';
import { delUser, queryLSUserPage, send, send2, updateUser } from './LogService';
import pagination from 'antd/lib/pagination';
import { ColumnProps } from 'antd/lib/table';
import { ColorResult } from 'react-color';
import { Link } from 'react-router-dom';
import { deleteUser } from '../../user/Service/UserService';
import Search from 'antd/lib/input/Search';
// import { queryRecordPage, saveRecord } from './LogService';
const confirm = Modal.confirm;
interface RecordListProp {
  id?: number;
}

interface LSUser {
  name: string,
  heroImg: string,
  userPhoto: string,
  lv: number,
  v1: number, v2: number, v3: number
}

interface ItemNode extends Partial<LSUser> {
  loading: boolean;
}

interface RecordPageState {
  id?: number;
  data: LSUser[],
  list: LSUser[],
  initLoading: boolean,
  loading: boolean,
  currentPage: number;
  reocrdVal?: string;
}
const defaultList: LSUser[] = [];
class RecordLog extends React.Component<RecordListProp, RecordPageState> {
  state: RecordPageState = {
    initLoading: true,
    loading: false,
    data: defaultList,
    list: defaultList,
    id: 0,
    currentPage: 1,
    reocrdVal: undefined,
  };



  componentDidMount() {
    message.config({
      top: 100,
      duration: 2,
      maxCount: 1,
    });


    this.initData();
  }




  columns: ColumnProps<LSUser>[] = [
    {
      title: '编号',
      dataIndex: 'index',
    }, {
      title: '昵称',
      dataIndex: 'name',

    },
    /**<Button type="primary" shape="circle"> */
    {
      title: '头像',
      dataIndex: 'userPhoto',
      render: (text: any, record: LSUser) => {
        return <img src={record.userPhoto} style={{ borderRadius: "50%" }} height={40} width={40} />;
      },
    },
    {
      title: '英雄名称',
      dataIndex: 'heroName',
    },
    {
      title: '英雄头像',
      dataIndex: 'heroImg',
      render: (text: any, record: LSUser) => {
        // const time = moment(record.createdTime).format("YYYY-MM-DD HH:mm:ss");
        return <img src={record.heroImg} style={{ borderRadius: "50%" }} height={40} width={40} />;
      },
    },
    {
      title: '血量',
      dataIndex: 'v1',
      render: (text: any, record: LSUser) => {
        // const time = moment(record.createdTime).format("YYYY-MM-DD HH:mm:ss");

        return <Search
          placeholder="输入值"
          defaultValue={text}
          enterButton={<Icon type="edit" />}
          onSearch={value => {
            record.v1 = Number(value); this.updateLsUser(record)
          }}
        />
      },
    },
    {
      title: '星级',
      dataIndex: 'v2',
      render: (text: any, record: LSUser) => {
        // const time = moment(record.createdTime).format("YYYY-MM-DD HH:mm:ss");
        record.v2 = text;
        return <Search
          placeholder="输入值"
          defaultValue={text}
          enterButton={<Icon type="edit" />}
          onSearch={value => {
            record.v2 = Number(value); this.updateLsUser(record)
          }}
        />
          ;
      },
    },
    {
      title: '护甲',
      dataIndex: 'v3',
      render: (text: any, record: LSUser) => {
        // const time = moment(record.createdTime).format("YYYY-MM-DD HH:mm:ss");
        record.v3 = text;
        return <Search
          placeholder="输入值"
          defaultValue={text}
          enterButton={<Icon type="edit" />}
          onSearch={value => {
            record.v3 = Number(value); this.updateLsUser(record)
          }}
        />
          ;
      },
    },
    // {
    //   title: '工程状态',
    //   dataIndex: 'status',
    // },
    {
      title: '操作',
      fixed: 'right',
      width: 200,
      render: (_text: any, record: LSUser) => (
        <Fragment >

          <Button size="small">
            <Link to={`/app/dy/DouyuFormIndex?name=${record.name}`}>编辑</Link>
          </Button>
          <Divider type="vertical" />
          <Button size="small" type="danger" onClick={() => {
            this.delete(record.name)
          }}
          >删除</Button>
        </Fragment>
      ),
    },
  ];



  initData() {

    queryLSUserPage().then((res: any) => {
      console.log(res);

      if (res.statusCode === '200' && res.result) {
        this.setState({
          initLoading: false,
          list: res.data ? res.data : [],
        });
      }
    })
  }

  updateLsUser = (vo: LSUser) => {
    updateUser(vo).then(res => {
      if (res.statusCode === '200') {
        message.success("更新成功");
        this.initData();
      }
    })
  }



  delete = (name: string) => {
    const _this = this;
    confirm({
      title: '提示',
      content: "确定删除该英雄位？",
      onOk() {
        console.log('OK');
        delUser({ name }).then(res => {
          if (res.statusCode === '200') {
            message.success("删除英雄成功！");
            _this.initData();
          } else {
            message.error("删除英雄失败！");
          }
        });
      },
      onCancel() {
        console.log('Cancel');
      },
    });
  }

  send() {
    send().then((res: any) => {
      console.log(res);

      if (res.statusCode === '200' && res.result) {
        message.success("同布数据到客服端成功");
      }
    })
  }


  onRecordValChange = ({ target: { value } }: any) => {
    this.setState({
      reocrdVal: value
    })
  }


  addUser() {
    console.log("addRecordLog");
    if (!this.state.reocrdVal) {
      message.warn("新增日志不能为空！");
      return;
    }
    // updateUser(this.state.id, this.state.reocrdVal!).then((res) => {
    //   if (res.resultCode === '200' && res.result) {
    //     message.info("新增记录成功！")
    //     this.initData(this.state.id!);
    //   }
    // });
  }

  keyGenerator = (vo: LSUser) => `${vo.name}`;
  render() {
    const height = window.innerHeight - 190;
    const { list, reocrdVal } = this.state;

    return (
      <div style={{ margin: "40px" }}>
        <Card style={{ margin: "16px", borderRadius: "4px", padding: "0px" }}>
          <Row style={{ margin: "10px" }}>

            <Button style={{ float: "right" }} type="primary">
              <Link to={`/app/dy/DouyuFormIndex`}>新增</Link>
            </Button>

            <Button onClick={() => { this.send() }} style={{ float: "right", marginRight: "20px" }} type="primary">
              同步数据
            </Button>

          </Row>
        </Card>
        <br />
        <Card style={{ margin: "0 16px", borderRadius: "4px", padding: "0px" }}>

          <Table
            rowKey={this.keyGenerator}
            columns={this.columns}
            dataSource={list}
          />
        </Card>
      </div >
    );
  }
}

export default RecordLog;
