
import { Card, Input, Button, Row, Col } from 'antd';
import React from 'react';
import { queryLSUserPage } from './LogService';
import { webSocket } from 'rxjs/webSocket';
// import * as Sortable from "react-sortablejs";
// import QueueAnim from 'rc-queue-anim';
import './style.less';
declare let window: any;
declare let Sortable: any;

type DouyuProps = {
  // setAlitaState: (param: any) => void;
  // id?: number;
}

export interface LSUser {
  name: string;
  heroImg: string,
  heroName?: string,
  heroCode?: string,
  userPhoto: string,
  lv: number,
  v1: number, v2: number, v3: number,
  // id?: string;
}

interface DouyuState {
  ip?: string;
  list: LSUser[];
}

const map = new Map<string, string>();
const oldIndexTemp = new Map<string, number>();
const newIndexTemp = new Map<string, number>();
class DouyuPage extends React.Component<DouyuProps, DouyuState> {

  state = {
    ip: undefined,
    list: []
  }


  componentWillMount() {

  };
  componentDidMount() {
    this.initData();
  };




  put(name: string, clz: string) {
  }


  log() {
  }


  initData() {
    queryLSUserPage().then((res: any) => {
      if (res.statusCode === '200' && res.result) {
        this.setState({
          list: res.data ? res.data.map((item: LSUser) => ({ ...item, id: item.name })) : [],
        });
        this.initWS();
      }
    })
  }

  initWS() {
    console.log('init ws')
    const el: HTMLElement | any = document.getElementById('st')
    window.sortable = Sortable.create(el, {
      animation: 500,
    });
    const token = Math.random() * 100 + 1;
    const ws = webSocket<LSUser[]>('ws://47.104.241.253:8081?token=' + token);
    ws.subscribe((res: LSUser[]) => {
      const list = res.map(item => ({ ...item, id: item.name }))
      list.sort((a: LSUser, b: LSUser) => {
        return a.v1 <= b.v1 ? 1 : -1
      })
      window.sortable.sort(list.map(r => r.name), true); // apply
      setTimeout(() => {
        this.setState({ list })
      }, 500)
    });
  }


  renderList() {
    const { list } = this.state;
    this.setState({ list });
  }

  render() {
    const list: LSUser[] = this.state.list;
    list.sort((a: LSUser, b: LSUser) => {
      return a.v1 <= b.v1 ? 1 : -1
    })
    return (
      <div id="st" style={{ margin: "40px" }}>
        {
          list.map((item, index) => {
            if (!item) {
              item = {
                name: "NULL" + Math.random(),
                heroImg: "./././././douyu/h1.png",
                userPhoto: "./././././douyu/tm.jpeg",
                lv: 1,
                v1: 30, v2: 20, v3: 30
              }
            }
            return <Row data-id={item.name} key={item.name} className={map.get(item.name)} style={{ backgroundImage: "url('./././././douyu/bg.png')", width: "420px", height: "50px" }}>
              <Col span={3}>
                <div style={{
                  width: "100%",
                  height: "42px",
                  backgroundImage: "url('./././././douyu/lv.png')",
                  backgroundSize: "auto 42px",
                  backgroundRepeat: "no-repeat",
                }}
                >
                  <span style={{
                    width: "100%",
                    height: "42px",
                    lineHeight: "42px",
                    textAlign: "center",
                    fontSize: "24px",
                    marginLeft: "10px",
                    marginTop: "-6px",
                    color: "#FFA500",
                  }}
                  >
                    {index + 1}
                  </span>
                </div>
              </Col>
              <Col span={2} >
                <img style={{
                  width: "100%",
                  height: "42px",
                }}
                  src={item.userPhoto}
                />
              </Col>
              <Col span={2} >
                <img style={{
                  width: "24px",
                  height: "24px", margin: "2px 5px"
                }} src="./././././douyu/ls.png"
                />
              </Col>
              <Col span={12}>
                <Row style={{ fontSize: "21px", fontWeight: "normal", color: "#FFA500", height: "25px", lineHeight: "25px" }}>{item.name}</Row>
                <Row style={{ fontSize: "12px", fontWeight: "bold", color: "#FFF", height: "25px", lineHeight: "25px" }}>

                  <Col style={{ height: "25px", lineHeight: "25px" }} span={8}>
                    <img style={{ margin: "2px 1px" }} height={18} src="./././././douyu/xueliang.png" />
                    <span style={{ fontSize: "12px", }}> {item.v1}</span>
                  </Col>

                  <Col style={{ height: "25px", lineHeight: "25px" }} span={8}>
                    <img style={{ margin: "2px 1px" }} height={18} src="./././././douyu/wujiaox.png" />
                    <span style={{ fontSize: "12px", }}> {item.v2}</span>
                  </Col>

                  <Col style={{ height: "25px", lineHeight: "25px" }} span={8}>
                    <img style={{ margin: "2px 1px" }} height={18} src="./././././douyu/dun.png" />
                    <span style={{ fontSize: "12px", }}> {item.v3}</span>
                  </Col>
                </Row>
              </Col>

              <Col span={5}>
                <img height={50} src={item.heroImg} />
              </Col>
            </Row>
          })
        }
      </div>
    );
  }
}

export default DouyuPage;
{/* <ReactSortable
animation={500}
list={list}
setList={(newState) => this.setState({ list: newState })}
>
{list.map((item: LSUser, index: number) => (
  <Row key={item.name} className={map.get(item.name)} style={{ backgroundImage: "url('./././././douyu/bg.png')", width: "420px", height: "50px" }}>
    <Col span={3}>
      <div style={{
        width: "100%",
        height: "42px",
        backgroundImage: "url('./././././douyu/lv.png')",
        backgroundSize: "auto 42px",
        backgroundRepeat: "no-repeat",
      }}
      >
        <span style={{
          width: "100%",
          height: "42px",
          lineHeight: "42px",
          textAlign: "center",
          fontSize: "24px",
          marginLeft: "10px",
          marginTop: "-6px",
          color: "#FFA500",
        }}
        >
          {index + 1}
        </span>
      </div>
    </Col>
    <Col span={2} >
      <img style={{
        width: "100%",
        height: "42px",
      }}
        src={item.userPhoto}
      />
    </Col>
    <Col span={2} >
      <img style={{
        width: "24px",
        height: "24px", margin: "2px 5px"
      }} src="./././././douyu/ls.png"
      />
    </Col>
    <Col span={12}>
      <Row style={{ fontSize: "21px", fontWeight: "normal", color: "#FFA500", height: "25px", lineHeight: "25px" }}>{item.name}</Row>
      <Row style={{ fontSize: "12px", fontWeight: "bold", color: "#FFF", height: "25px", lineHeight: "25px" }}>

        <Col style={{ height: "25px", lineHeight: "25px" }} span={8}>
          <img style={{ margin: "2px 1px" }} height={18} src="./././././douyu/xueliang.png" />
          <span style={{ fontSize: "12px", }}> {item.v1}</span>
        </Col>

        <Col style={{ height: "25px", lineHeight: "25px" }} span={8}>
          <img style={{ margin: "2px 1px" }} height={18} src="./././././douyu/wujiaox.png" />
          <span style={{ fontSize: "12px", }}> {item.v2}</span>
        </Col>

        <Col style={{ height: "25px", lineHeight: "25px" }} span={8}>
          <img style={{ margin: "2px 1px" }} height={18} src="./././././douyu/dun.png" />
          <span style={{ fontSize: "12px", }}> {item.v3}</span>
        </Col>
      </Row>
    </Col>

    <Col span={5}>
      <img height={50} src={item.heroImg} />
    </Col>
  </Row>
))}
</ReactSortable> */}