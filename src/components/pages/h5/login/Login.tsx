/**
 * Created by hao.cheng on 2017/4/16.
 */
import React from 'react';
import {Button, Checkbox, Form, Icon, Input, message, Row} from 'antd';

import {connectAlita} from 'redux-alita';
import {RouteComponentProps} from 'react-router';
import {FormProps} from 'antd/lib/form';
import umbrella from 'umbrella-storage';
import {login} from '../../../../axios/comm';
import './style.less';


const FormItem = Form.Item;
type LoginProps = {
  setAlitaState: (param: any) => void;
  auth: any;
} & RouteComponentProps &
  FormProps;
class Login extends React.Component<LoginProps> {
  componentDidMount() {
    const { setAlitaState } = this.props;
    setAlitaState({ stateName: 'auth', data: null });
  }
  componentDidUpdate(prevProps: LoginProps) {
    // React 16.3+弃用componentWillReceiveProps
    const { auth: nextAuth = {}, history } = this.props;
    // const { history } = this.props;
    console.log("auth", nextAuth);
    if (nextAuth.data && nextAuth.data.token) {
      // 判断是否登陆
      umbrella.setLocalStorage('user', nextAuth.data);
      message.info("登录成功");
      //history.push('/app/h5/index');
      history.goBack();
    }
  }
  handleSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    this.props.form!.validateFields((err, values) => {
      if (!err) {
        const { setAlitaState } = this.props;

        login(values.userName, values.password).then(res => {
          const data = res.result;
          console.log("login", data);

          setAlitaState({ data, stateName: 'auth' });
        });
      }
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form!;
    return (
      <div className="homeBackground" style={{ height: "100%", marginTop: "-48px", backgroundImage: "url('./././././images/Login_background.jpg')" }}>

        <Row className="appTitle">电子标识系统</Row>
        <Icon onClick={() => {
          window.history.back();
        }}
          style={{ color: "#fff", position: "absolute", left: "20px", top: "20px" }} type="left"
        />
        <Form onSubmit={this.handleSubmit} >
          <Row className="loginBox">
            <FormItem>
              {getFieldDecorator('userName', {
                rules: [{ required: true, message: '请输入用户名!' }],
              })(
                <Input
                  className="loginInput"
                  style={{ borderRadius: "4px", height: '40px', backgroundColor: "rgba(255, 255, 255, 0.6)" }}
                  prefix={<Icon type="user" style={{ fontSize: 13 }} />}
                  placeholder="用户名"
                />
              )}
            </FormItem>
            <FormItem>
              {getFieldDecorator('password', {
                rules: [{ required: true, message: '请输入密码!' }],
              })(
                <Input
                  className="loginInput"
                  style={{ borderRadius: "4px", height: '40px', backgroundColor: "rgba(255, 255, 255, 0.6)" }}
                  prefix={<Icon type="lock" style={{ fontSize: 13 }} />}
                  type="password"
                  placeholder="密码"
                />
              )}
            </FormItem>
            <FormItem style={{ color: "#fff" }}>
              {getFieldDecorator('remember', {
                valuePropName: 'checked',
                initialValue: true,
              })(<Checkbox style={{ color: "#fff" }}>记住我</Checkbox>)}
              <span onClick={() => { message.info("请联系管理人员"); }} className="login-form-forgot" style={{ float: 'right' }}>
                忘记密码
              </span>

            </FormItem>

          </Row>

          <Row>
            <Button
              type="primary"
              htmlType="submit"
              className="login-form-button"
              style={{ margin: "20px 12%", width: '76%', backgroundColor: "#fff", color: "#566ee9", borderRadius: "8px", height: "42px", fontWeight: "bold", fontSize: "20px", border: "1px #fff" }}
            >
              登录
            </Button>
          </Row>
        </Form>




        {/*
        <Row>
          <div className="login-form" style={{ backgroundColor: "rgba(97, 117, 233, 0.6)" }}>




            <Form onSubmit={this.handleSubmit} style={{ maxWidth: '300px' }}>
              <FormItem>
                {getFieldDecorator('userName', {
                  rules: [{ required: true, message: '请输入用户名!' }],
                })(
                  <Input
                    prefix={<Icon type="user" style={{ fontSize: 13 }} />}
                    placeholder="请输入账号"
                  />
                )}
              </FormItem>
              <FormItem>
                {getFieldDecorator('password', {
                  rules: [{ required: true, message: '请输入密码!' }],
                })(
                  <Input
                    prefix={<Icon type="lock" style={{ fontSize: 13 }} />}
                    type="password"
                    placeholder="请输入密码"
                  />
                )}
              </FormItem>
              <FormItem>
                {getFieldDecorator('remember', {
                  valuePropName: 'checked',
                  initialValue: true,
                })(<Checkbox>记住我</Checkbox>)}
                <span className="login-form-forgot" style={{ float: 'right' }}>
                  忘记密码
                </span>
                <Button
                  type="primary"
                  htmlType="submit"
                  className="login-form-button"
                  style={{ width: '100%' }}
                >
                  登录
                </Button>
              </FormItem>
            </Form>
          </div>


        </Row> */}
      </div>
    );
  }
}

export default connectAlita(['auth'])(Form.create()(Login));

