import React from 'react';


class LoginSuccess extends React.Component {

  render() {
    return (
      <div
        className="center"
        style={{ height: '100%', background: '#ececec', overflow: 'hidden' }}
      >
        <h1>登录成功！</h1>
      </div>
    );
  }
}

export default LoginSuccess;
