import React, { FormEvent } from 'react';
import { Button, Card, DatePicker, Input, message, Modal, Row, Select } from 'antd';

import Form, { FormProps } from 'antd/lib/form';
import './style.less';
import { RouteComponentProps } from 'react-router-dom';
import { queryString } from '../../../../utils';

import { ColorResult, SketchPicker } from 'react-color';
import TextArea from 'antd/lib/input/TextArea';


import UploadImg from '../../../comm/file/UploadImg';
import { getLSHero, Hero, updateHero } from './HeroService';

const { RangePicker } = DatePicker;
const dateFormat = 'YYYY/MM/DD';
const confirm = Modal.confirm;

const FormItem = Form.Item;
const Option = Select.Option;


type HeroFormProps = {} & FormProps & RouteComponentProps;

interface HeroFormState extends Partial<Hero> {
  id?: number;
}

class HeroFormIndex extends React.Component<HeroFormProps, HeroFormState> {
  constructor(props: any) {
    super(props);
    this.state = {

    };
  }

  componentDidMount() {
    var param = queryString();
    const code = param.code;
    this.setState({ code });
    this.initForm(code);

  }

  initForm(code: string) {
    let values: Partial<Hero> = {};

    code && getLSHero(code!).then(res => {
      if (res.data) {
        values = res.data;
        this.setState({
          ...values
        })
        this.renderForm(values);
      }
    });
    this.renderForm(values);
  }

  renderForm(fieldsValue: Partial<HeroFormState>) {

    this.props.form!.setFieldsValue(fieldsValue, () => {

    });
  }

  onUploadFinish(id: number, name: string) {
    console.log(id, name);

    // let { imgList } = this.state;
    // imgList.push({ id, url: name })
    // this.setState({
    //   imgList
    // })
  }

  uploadScene() {
    // const { imgList, id } = this.state;
    // saveIdentificationImg(imgList, parseInt(id!)).then(res => {
    //   if (res.result) {
    //     message.info("上传成功！");
    //     this.init(parseInt(id!));
    //   }
    // });
  }

  formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 8 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 14 },
    },
  };



  handleSubmit = (e: FormEvent) => {
    e.preventDefault();

    this.props.form!.validateFields((err, values) => {
      if (!err) {
        console.log("handleSubmit", values);
        //如果图片无改动，不用再次上传。

        const data: Hero = {
          ...values,
          heroImg: this.state.heroImg
        }
        if (values.validTime && values.validTime.length === 2) {

        }

        this.updateHero(data);
      };
    });
  };

  onUploadFinishReturn(id: string, name: string, url: string) {
    console.log(id, name, url);
    this.setState({
      heroImg: url
    })
  }

  updateHero(vo: Partial<Hero>) {
    updateHero(vo).then(res => {
      if (res.statusCode === '200') {
        this.props.history.goBack();
        message.success("更新成功");
      }
    })
  }


  render() {
    const { form } = this.props;

    const {
      // appCode,
      code, heroImg
    } = this.state;
    const { getFieldDecorator } = form!;
    console.log("itemFormRender state:", this.state);


    return (
      <div>
        <Card title="编辑">
          <Form onSubmit={this.handleSubmit}>
            <FormItem {...this.formItemLayout} label="CODE">
              {getFieldDecorator('code', {
                rules: [{ required: true, message: '请输入code!' }],
              })(<Input style={{ width: "300px" }} placeholder="请输入code" />)}
            </FormItem>

            <FormItem {...this.formItemLayout} label="英雄名称">
              {getFieldDecorator('heroName', {
                rules: [{ message: '请输入!' }],
              })(<Input style={{ width: "300px" }} placeholder="请输入" />)}
            </FormItem>

            <FormItem {...this.formItemLayout} label="英雄">
              <UploadImg
                defaultImgUrl={heroImg}
                onUploadFinish={this.onUploadFinish.bind(this)}
                onUploadFinishReturn={this.onUploadFinishReturn.bind(this)}
              />
            </FormItem>

            <Row className="submitAction">
              <Button type="primary" htmlType="submit">提交</Button>
              <Button onClick={() => {
                this.props.history.goBack();
              }}
              >取消</Button>
            </Row>
          </Form>
        </Card>
      </div >
    );
  }
}

export default Form.create()(HeroFormIndex);

