import { zbGet, zbPost } from '../../../../axios/tools';


export interface Hero {
  code:string;
  heroName:string;
  heroImg:string;
}

export const getLSHero = (code:any) =>
    zbPost({
        url: '/douyu/hero/get',
        data: {code},
        msg: '',
    });


  export const queryHeroPage = () =>
    zbPost({
        url: '/douyu/hero/getAll',
        data: {},
        msg: '',
    });


export const deleteHero = (Hero:Partial<Hero>) =>
zbPost({
    url: '/douyu/hero/delete',
    data: Hero,
    msg: '',
});

export const updateHero = (Hero:Partial<Hero>) =>
    zbPost({
        url: '/douyu/hero/update',
        data: Hero,
        msg: '',
    });
 