import React, { Fragment } from 'react';
import { Button, Card, Col, Divider, Input, List, message, Modal, Row, Skeleton, Table } from 'antd';
import './style.less';

import moment from 'moment';
import { queryString } from '../../../../utils';
import umbrella from 'umbrella-storage';

import pagination from 'antd/lib/pagination';
import { ColumnProps } from 'antd/lib/table';
import { ColorResult } from 'react-color';
import { Link } from 'react-router-dom';
import { deleteHero, Hero, queryHeroPage } from './HeroService';
const confirm = Modal.confirm;
// import { queryRecordPage, saveRecord } from './LogService';

interface RecordListProp {
  id?: number;
}


interface ItemNode extends Partial<Hero> {
  loading: boolean;
}

interface RecordPageState {
  id?: number;
  data: Hero[],
  list: Hero[],
  initLoading: boolean,
  loading: boolean,
  currentPage: number;
  reocrdVal?: string;
}
const defaultList: Hero[] = [];
class HeroAdmin extends React.Component<RecordListProp, RecordPageState> {
  state: RecordPageState = {
    initLoading: true,
    loading: false,
    data: defaultList,
    list: defaultList,
    id: 0,
    currentPage: 1,
    reocrdVal: undefined,
  };



  componentDidMount() {
    message.config({
      top: 100,
      duration: 2,
      maxCount: 1,
    });


    this.initData();
  }


  delete = (code: string) => {
    const _this = this;
    confirm({
      title: '提示',
      content: "确定删除该英雄位？",
      onOk() {
        console.log('OK');
        deleteHero({ code }).then(res => {
          if (res.statusCode === '200') {
            message.success("删除英雄成功！");
            _this.initData();
          } else {
            message.error("删除英雄失败！");
          }
        });
      },
      onCancel() {
        console.log('Cancel');
      },
    });
  }


  columns: ColumnProps<Hero>[] = [
    {
      title: '英雄编号',
      dataIndex: 'code',
    }, {
      title: '英雄名称',
      dataIndex: 'heroName',

    },
    /**<Button type="primary" shape="circle"> */
    {
      title: '英雄皮肤',
      dataIndex: 'userPhoto',
      render: (text: any, record: Hero) => {
        return <img src={record.heroImg} style={{ borderRadius: "10%" }} height={40} width={40} />;
      },
    },
    {
      title: '操作',
      fixed: 'right',
      width: 300,
      render: (_text: any, record: Hero) => (
        <Fragment >

          <Button size="small">
            <Link to={`/app/hero/HeroFormIndex?code=${record.code}`}>编辑</Link>
          </Button>
          <Divider type="vertical" />
          <Button size="small" type="danger" onClick={() => {
            this.delete(record.code)
          }}
          >删除</Button>
        </Fragment>
      ),
    },
  ];



  initData() {

    queryHeroPage().then((res: any) => {
      console.log(res);

      if (res.statusCode === '200' && res.result) {
        this.setState({
          initLoading: false,
          list: res.data ? res.data : [],
        });
      }
    })
  }



  onRecordValChange = ({ target: { value } }: any) => {
    this.setState({
      reocrdVal: value
    })
  }


  addUser() {
    console.log("addRecordLog");
    if (!this.state.reocrdVal) {
      message.warn("新增日志不能为空！");
      return;
    }
    // updateUser(this.state.id, this.state.reocrdVal!).then((res) => {
    //   if (res.resultCode === '200' && res.result) {
    //     message.info("新增记录成功！")
    //     this.initData(this.state.id!);
    //   }
    // });
  }

  keyGenerator = (vo: Hero) => `${vo.code}`;
  render() {
    const height = window.innerHeight - 190;
    const { list, reocrdVal } = this.state;

    return (
      <div style={{ margin: "40px" }}>
        <Card style={{ margin: "16px", borderRadius: "4px", padding: "0px" }}>
          <Row style={{ margin: "10px" }}>

            <Button style={{ float: "right" }} type="primary">
              <Link to={`/app/hero/HeroFormIndex`}>新增</Link>
            </Button>
          </Row>
        </Card>
        <br />
        <Card style={{ margin: "0 16px", borderRadius: "4px", padding: "0px" }}>

          <Table
            rowKey={this.keyGenerator}
            columns={this.columns}
            dataSource={list}
          />
        </Card>
      </div >
    );
  }
}

export default HeroAdmin;

