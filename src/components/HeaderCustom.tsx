/**
 * Created by hao.cheng on 2017/4/13.
 */
import React, { Component } from 'react';
import SiderCustom from './SiderCustom';
import { Menu, Icon, Layout, Badge, Popover, Button } from 'antd';
import { queryString } from '../utils';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import { PwaInstaller, ThemePicker } from './widget';
import { connectAlita } from 'redux-alita';
import umbrella from 'umbrella-storage';
import { getCurrentUser } from '../axios/comm';
const { Header } = Layout;
const SubMenu = Menu.SubMenu;
const MenuItemGroup = Menu.ItemGroup;

type HeaderCustomProps = RouteComponentProps<any> & {
  toggle: () => void;
  collapsed: boolean;
  user: any;
  responsive?: any;
  path?: string;
  onEvent?: (eventName: string, obj: any) => void;
};
type HeaderCustomState = {
  user: any;
  visible: boolean;
  isH5Path: boolean;
  headerVisible: boolean;
};

class HeaderCustom extends Component<HeaderCustomProps, HeaderCustomState> {
  state = {
    user: { user: { loginName: "" }, userInfo: { realName: "" } },
    visible: false,
    isH5Path: false,
    headerVisible: true
  };
  componentDidMount() {
    const QueryString = queryString() as any;
    let storageUser = umbrella.getLocalStorage('user');
    console.log("storageUser", storageUser);

    this.setState({
      isH5Path: this.props.history.location.pathname.search("app/h5") != -1
    });
    // _user = (storageUser && JSON.parse(storageUser)) || '测试';
    // if (!storageUser && QueryString.hasOwnProperty('code')) {
    //   getCurrentUser().then(res => {
    //     this.setState({
    //       user: res.result,
    //     });
    //     umbrella.setLocalStorage('user', res.result);
    //   });
    // } else {
    //   this.setState({
    //     user: storageUser,
    //   });
    // }
  }

  menuClick = (e: { key: string }) => {
    // e.key === 'logout' && this.logout();
  };
  // logout = () => {
  //   umbrella.removeLocalStorage('user');
  //   this.props.history.push('/login');
  // };
  // login = () => {
  //   umbrella.removeLocalStorage('user');
  //   this.props.history.push('/app/h5/login');
  // };
  popoverHide = () => {
    this.setState({
      visible: false,
    });
  };
  handleVisibleChange = (visible: boolean) => {
    this.setState({ visible });
  };
  render() {
    const { isH5Path, headerVisible } = this.state;
    const { responsive = { data: {} } } = this.props;
    console.log("isH5Path:", isH5Path);
    let name;
    try {
      name = (this.state.user.userInfo && this.state.user.userInfo.realName) ? this.state.user.userInfo.realName : this.state.user.user.loginName;
    } catch (e) {
      name = "游客";
    }

    return (

      <Header style={{ display: headerVisible ? "" : "none" }} className="custom-theme header">

        {responsive.data.isMobile ?
          (
            <Popover
              content={<SiderCustom popoverHide={this.popoverHide} />}
              trigger="click"
              placement="bottomLeft"
              visible={this.state.visible}
              onVisibleChange={this.handleVisibleChange}
            >
              <Icon type="bars" className="header__trigger custom-trigger" />
            </Popover>
          )
          :
          (
            <Icon
              className="header__trigger custom-trigger"
              type={this.props.collapsed ? 'menu-unfold' : 'menu-fold'}
              onClick={this.props.toggle}
            />
          )}
        <Button type="primary" onClick={() => { this.props.onEvent!("HIDE", "OK"); this.setState({ headerVisible: false }) }}>隐藏</Button>
        <Button type="primary" onClick={() => { this.props.onEvent!("LAYOUT_COLOR", "#FFFFFF") }}>白色</Button>
        <Button type="primary" onClick={() => { this.props.onEvent!("LAYOUT_COLOR", "#00FF00") }}>绿色</Button>

        {/* <ThemePicker /> */}
        <Menu
          mode="horizontal"
          style={{ lineHeight: '64px', float: 'right' }}
          onClick={this.menuClick}
        >
          <Menu.Item key="pwa">
            <PwaInstaller />
          </Menu.Item>

          <Menu.Item key="1">
          </Menu.Item>
          <SubMenu
            title={
              name
            }
          >
            <MenuItemGroup title="用户中心">
              <Menu.Item key="setting:1">你好 - {name}</Menu.Item>
              {/* <Menu.Item key="setting:2">个人信息</Menu.Item> */}

              {
                // name === '游客' ?
                //   <Menu.Item key="login">
                //     <span onClick={this.login}>登录</span>
                //   </Menu.Item> :
                //   <Menu.Item key="logout">
                //     <span onClick={this.logout}>退出登录</span>
                //   </Menu.Item>
              }

              {/* <Menu.Item key="logout">
                <span onClick={this.logout}>退出登录</span>
              </Menu.Item> */}
            </MenuItemGroup>
            {/* <MenuItemGroup title="设置中心">
              <Menu.Item key="setting:3">个人设置</Menu.Item>
              <Menu.Item key="setting:4">系统设置</Menu.Item>
            </MenuItemGroup> */}
          </SubMenu>
        </Menu>
      </Header>
    );
  }
}

// 重新设置连接之后组件的关联类型
const HeaderCustomConnect: React.ComponentClass<
  HeaderCustomProps,
  HeaderCustomState
> = connectAlita(['responsive'])(HeaderCustom);

export default withRouter(HeaderCustomConnect);
