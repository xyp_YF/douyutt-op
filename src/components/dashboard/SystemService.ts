import { zbGet } from "../../axios/tools";

export interface StatisticsSystem{
  usetTotal:number;
  projectTotal:number;
  idTotal:number;
}


export const queryStatisticsSystem = () =>
    zbGet({
        url: '/ei/statistics/system',
        msg: '',
    });
