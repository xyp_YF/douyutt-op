/**
 * Created by hao.cheng on 2017/5/3.
 */
import React from 'react';
import { Row, Col, Card, Timeline, Icon } from 'antd';
import BreadcrumbCustom from '../BreadcrumbCustom';
import { queryStatisticsSystem, StatisticsSystem } from './SystemService';
import umbrella from 'umbrella-storage';

interface DashboardState {
  system: Partial<StatisticsSystem>;
  isAdmin: boolean;
}
class Dashboard extends React.Component<{}, DashboardState> {
  state = {
    system: { idTotal: 0, userTotal: 0, projectTotal: 0 }, isAdmin: false
  }
  componentDidMount() {

  }

  render() {
    const { system, isAdmin } = this.state;
    return (
      <div style={{ padding: "20px" }} className="gutter-example button-demo" />
    )
  }
}

export default Dashboard;
