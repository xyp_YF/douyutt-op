import React, { Component } from 'react';
import { Icon, Upload } from 'antd';
import { HttpRequestHeader, UploadChangeParam } from 'antd/lib/upload/interface';
import './style.less';

export interface UploadParam {
  token: string;
  key: string;
  // host: string;
}

export interface UploadProp {
  // getUploadParam: () => void;
  onUploadFinish: (id: number, name: string) => void;
  onUploadFinishReturn?: (id: string, key: string, name: string) => void;
  // 默认显示图片
  defaultImgUrl?: string;
}

export interface UploadState {
  imageUrl: string;
  loading: boolean;
  token: string;
  key: string;
  host: string;
}
const baseUrl = process.env.NODE_ENV === 'development' ? '/op' : 'http://47.104.241.253:8080';
class UploadImg extends Component<UploadProp, UploadState> {
  state = {
    imageUrl: '',
    loading: false,
    token: '',
    key: '',
    host: baseUrl + "/uploadFile"
    // host: '',
  };


  // 在请求上传时，更新请求七牛的参数，而不是在创建组件时请求。
  beforeUpload = () => {
    // 未使用awit。方法beforeUpload被声明为异步后，会报错
    const id = "1";

  }

  componentDidMount() {
    console.log("componentDidMount");
  };

  handleChange = (info: UploadChangeParam) => {
    console.log("handleChange:", info)
    if (info.file.status === 'uploading') {
      this.setState({ loading: true });
      return;
    }
    console.log("UploadhandleChange", info)
    // debugger;
    if (info.file.status === 'done' && info.file.response.id) {
      this.getBase64(info.file.originFileObj, (imageUrl: string) => {
        this.setState({
          imageUrl,
          loading: false,
        })
      }
      )
      //name
      this.props.onUploadFinish(info.file.response.id, info.file.response.url);

      if (this.props.onUploadFinishReturn) {
        this.props.onUploadFinishReturn!(info.file.response.id, info.file.response.fileName, info.file.response.url);
      }
    }
  }

  getBase64 = (img?: Blob, callback?: any) => {
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result));
    reader.readAsDataURL(img as Blob);
  }

  render() {
    console.log("render");
    const { defaultImgUrl } = this.props;
    const { token, key, host } = this.state;
    const data = { "X-EI-AppCode": "101" };
    // const header: HttpRequestHeader = { "X-EI-AppCode": " 101" };


    const { imageUrl } = this.state;

    let url = null;
    if (imageUrl) {
      url = imageUrl;
    } else if (defaultImgUrl) {
      //TODO 让后端改
      url = defaultImgUrl;
    }

    const uploadButton = (
      <div>
        <Icon type={this.state.loading ? 'loading' : 'plus'} />
        <div className="ant-upload-text">上传</div>
      </div>
    );
    return (
      <div>
        <Upload
          name="file"
          listType="picture-card"
          className="avatar-uploader"
          showUploadList={false}
          data={data}
          action={host}
          onChange={this.handleChange}
          // headers={header}
          style={{ width: "200px", color: "#5a5a5a", fontSize: "16px", fontWeight: 500 }}
        >
          {url ? <img src={url} alt="avatar" style={{ width: '100%' }} /> : uploadButton}
        </Upload>
      </div >
    );
  }
}

export default UploadImg;
