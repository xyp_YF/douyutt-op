import { post } from '../../../axios/tools';

export async function getUploadImgParam(uploadParamList: QiniuUploadParam[]) {
    return post({
        url: '/callback/comm/file/qiniu/upload/getUploadFileParam',
        data: { uploadParamList },
        msg: '',
    });
}

export const getUploadParam = (path: string) =>
    post({ url: '/callback/comm/file/qiniu/upload/getUploadFileParam', data: { path }, msg: '' });

export interface QiniuUploadParam {
    id: any;
    isPrivate: boolean;
    module: string;
    suffix: string;
}
