import { Upload, Modal, Icon, Button } from 'antd';
// import { PlusOutlined } from '@ant-design/icons';
import React from 'react';
import { UploadFile, UploadChangeParam } from 'antd/lib/upload/interface';
import { getUploadImgParam, QiniuUploadParam } from './FileService';
function getBase64(file: Blob) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  });
}

export interface ImgVO {
  uid: string;
  size: number;
  name: string;
  fileName?: string;
  url?: string;
  thumbUrl?: string;
  //附件id
  attachmentUrl?: string;
  attachmentId?: number;
  imgUrl?: string;

}
//Array<UploadFile>

export interface UploadProp {
  // getUploadParam: () => void;
  onUploadFinish: (img: Array<ImgVO>) => void;
  // 默认显示图片
  defaultImgUrl?: string;
  isPrivate: boolean;
  suffix: string;
  module: string;
  imgList: Partial<ImgVO>[];
}

export interface UploadImgState {
  previewVisible: boolean;
  previewImage: string;
  previewTitle: string;
  fileList: Array<Partial<UploadFile>>;
  host: string;
  token: string;
  key: string;
  isShowAllImg: boolean;
}

export class QiniuUploadImgList extends React.Component<UploadProp, UploadImgState>{
  state = {
    previewVisible: false,
    previewImage: '',
    previewTitle: '',
    fileList: [],
    host: "",
    token: "",
    key: "",
    isShowAllImg: false
  };

  componentDidMount() {
    const { imgList = [] } = this.props;

    // console.log("imgList", imgList);

    // this.setState({
    //   fileList: imgList
    // })
  }

  handleCancel = () => this.setState({ previewVisible: false });

  handlePreview = async (file: UploadFile) => {
    if (!file.url) {
      // file.preview = await getBase64(file.originFileObj);
    }

    this.setState({
      previewImage: file.url as string,
      previewVisible: true,
      previewTitle: file.name || (file.url as string).substring((file.url as string).lastIndexOf('/') + 1),
    });
  };

  // 在请求上传时，更新请求七牛的参数，而不是在创建组件时请求。
  beforeUpload = () => {
    // 未使用awit。方法beforeUpload被声明为异步后，会报错
    const id = "1";
    const { isPrivate, suffix, module } = this.props;
    const param: QiniuUploadParam = {
      isPrivate, suffix, module, id
    }
    return getUploadImgParam([param]).then(res => {
      const { result, statusCode } = res;
      //  result:{key,host,token}
      if (statusCode === '200' && result.results) {
        const data = result.results[id];
        console.log("beforeUpload", data);
        this.setState({
          ...data,
        });
      }
    });
  }

  showAllImg = () => {
    this.setState({ isShowAllImg: true })

  }

  handleChange = ({ fileList }: UploadChangeParam) => {
    this.setState({ fileList });
    console.log("handleChange", fileList);
    const list = fileList.map((file) => {
      if (file.url) {

        const imgVO: ImgVO = {
          ...file,
          attachmentUrl: file.url,
          attachmentId: parseInt(file.uid)
        }
        return imgVO;

      } else {
        const imgVO: ImgVO = {
          ...file,
          attachmentUrl: file.response ? file.response.result.qiniuUrl : undefined,
          attachmentId: file.response ? file.response.result.id : undefined,
        }
        return imgVO;
      }

    });
    this.props.onUploadFinish(list);
  }

  render() {
    const { previewVisible, token, key, isShowAllImg, previewImage, fileList, previewTitle, host } = this.state;
    const { imgList } = this.props;
    const data = { token, key };
    const uploadButton = (
      <div>
        <Icon type="plus" />
        <div className="ant-upload-text">Upload</div>
      </div>
    );
    console.log("imgList", imgList);
    let defaultFiles: UploadFile[] = [];
    if (imgList && imgList.length > 0) {
      defaultFiles = imgList.map((img: Partial<ImgVO>, index: number) => {
        //TODO 优化 解决循环出现  "-list_big" 的原因
        let url = (img.imgUrl || img.url);
        url = url ? url!.replace("-list_big", "") + "-list_big" : undefined;
        const file: UploadFile = {
          uid: img.uid || img.attachmentId!.toString(),
          size: 100,
          name: (index + ".png"),
          url,
          type: "image/jpeg",
        }
        return file;
      })
    }
    /**
     * 如果state的数据，有值，展示出啦，没值，展示默认值
     * 如果先展示了默认值，在选择，则新的onChange的参数，必然包含了原有的数据
     */
    // const list = (fileList && fileList.length > 0) ? fileList : defaultFiles;
    // console.log(fileList, defaultFiles);
    // console.log("fileList", fileList);
    return (
      <div className="clearfix" >
        <Upload
          action={host}
          data={data}
          listType="picture-card"
          fileList={defaultFiles}
          // defaultFileList={defaultFiles}
          onPreview={this.handlePreview}
          onChange={this.handleChange}
          beforeUpload={this.beforeUpload}
        >
          {fileList.length >= 8 ? null : uploadButton}
        </Upload>
        <div>
          <Button onClick={this.showAllImg}>预览商品详情</Button>
        </div>
        <Modal
          visible={previewVisible}
          title={previewTitle}
          footer={null}
          onCancel={this.handleCancel}
        >
          <img alt="example" style={{ width: '100%' }} src={previewImage} />
        </Modal>

        <Modal
          visible={isShowAllImg}
          title="预览"
          footer={null}
          onCancel={() => { this.setState({ isShowAllImg: false }) }}
        >
          {
            (defaultFiles && defaultFiles.length > 0) ? defaultFiles.map((file: UploadFile) => {
              //TODO 优化图片显示，这里加上了 -list_big,不然无法显示
              return <img key={file.name} alt="all" style={{ width: '100%' }} src={file.url} />
            }) : null
          }
        </Modal>
      </div>
    );
  }
}

export default QiniuUploadImgList;