import React, { Component } from 'react';
import { Icon, Upload } from 'antd';
import { UploadChangeParam } from 'antd/lib/upload/interface';
import { getUploadImgParam, QiniuUploadParam } from './FileService';

export interface UploadParam {
  token: string;
  key: string;
  // host: string;
}

export interface UploadProp {
  // getUploadParam: () => void;
  onUploadFinish: (id: number, name: string) => void;
  onUploadFinishReturn?: (id: number, key: string, name: string) => void;
  // 默认显示图片
  defaultImgUrl?: string;
  isPrivate: boolean;
  suffix: string;
  module: string;
}

export interface UploadState {
  imageUrl: string;
  loading: boolean;
  token: string;
  key: string;
  host: string;
}

class QiniuUploadImg extends Component<UploadProp, UploadState> {
  state = {
    imageUrl: '',
    loading: false,
    token: '',
    key: '',
    host: ""
    // host: '',
  };


  // 在请求上传时，更新请求七牛的参数，而不是在创建组件时请求。
  beforeUpload = () => {
    // 未使用awit。方法beforeUpload被声明为异步后，会报错
    const id = "1";
    const { isPrivate, suffix, module } = this.props;
    const param: QiniuUploadParam = {
      isPrivate, suffix, module, id
    }
    return getUploadImgParam([param]).then(res => {
      const { result, statusCode } = res;
      //  result:{key,host,token}
      if (statusCode === '200' && result.results) {
        const data = result.results[id];
        console.log("beforeUpload", data);
        this.setState({
          ...data,
        });
      }
    });
  }


  handleChange = (info: UploadChangeParam) => {
    if (info.file.status === 'uploading') {
      this.setState({ loading: true });
      return;
    }
    console.log("UploadhandleChange", info)
    if (info.file.status === 'done' && info.file.response.statusCode === '200') {
      // const name = info.file.response.result.name as string;
      //最新的契约 response里面有可供显示的url
      this.getBase64(info.file.originFileObj, (imageUrl: string) => {
        this.setState({
          imageUrl,
          loading: false,
        })
      }
      )
      //name
      this.props.onUploadFinish(info.file.response.result.id, info.file.response.result.qiniuUrl);

      if (this.props.onUploadFinishReturn) {
        this.props.onUploadFinishReturn!(info.file.response.result.id, info.file.response.result.name, info.file.response.result.qiniuUrl);
      }
    }
  }

  getBase64 = (img?: Blob, callback?: any) => {
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result));
    reader.readAsDataURL(img as Blob);
  }

  render() {
    const { defaultImgUrl } = this.props;
    const { token, key, host } = this.state;
    const data = { token, key };

    const { imageUrl } = this.state;

    let url = null;
    if (imageUrl) {
      url = imageUrl;
    } else if (defaultImgUrl) {
      //TODO 让后端改
      url = defaultImgUrl;
    }

    const uploadButton = (
      <div>
        <Icon type={this.state.loading ? 'loading' : 'plus'} />
        <div className="ant-upload-text">上传</div>
      </div>
    );
    return (
      <div>
        <Upload
          name="file"
          listType="picture-card"
          className="avatar-uploader"
          showUploadList={false}
          data={data}
          action={host}
          beforeUpload={this.beforeUpload}
          onChange={this.handleChange}
        >
          {url ? <img src={url} alt="avatar" style={{ width: '100%' }} /> : uploadButton}
        </Upload>
      </div>
    );
  }
}

export default QiniuUploadImg;
