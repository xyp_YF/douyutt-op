export interface ApplicationVO {
    id: number;
    name: string;
    alias: string;
    channelType: number;
    appCode: string;
}
