import { zbPost, zbGet } from '../../../axios/tools';

/**
 * 按条件查询入驻平台订单
 * @param param
 */
export const getAllApplication = () =>
    zbPost({ url: '/application/getAllApplication', data: {}, msg: '' });
