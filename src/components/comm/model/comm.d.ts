
/**
 * 分页
 */
export interface PageInfo {
    pageSize: number;
    currentPage: number;
    pageCount?: number;
    totalCount?: number;
}

export interface QueryPageReq{
  page:PageInfo
}