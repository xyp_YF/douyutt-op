import React, { Component } from 'react'
import './style.less';

interface TabNode {
  text: string;
  index?: number;
  img?: string;
}

let tarbarArr: TabNode[] = [
  {

    img: '././././images/home1',
    text: '首页'
  },
  {
    img: '././././images/Maintenance_records',
    text: '维护记录'
  },
  {
    img: '././././images/Landmark_management',
    text: '地标管理'
  },
  {
    img: '././././images/nearby',
    text: '附近'
  }
  ,
  {
    img: '././././images/set_up',
    text: '设置'
  }
]

interface TabProps {
  onClick: (node: TabNode) => void;
  defValue: number;
}

interface TabState {
  index: number;

}

/**
 * name
 */
class Tabbar extends Component<TabProps, TabState> {
  constructor(props: any) {
    super(props)
    const defValue = this.props.defValue;
    this.state = {
      index: defValue ? defValue : 0
    }
  }
  itemChange = (v: TabNode, i: number) => {
    this.setState({
      index: i
    })

    this.props.onClick({ ...v, index: i });
  }
  render() {
    return (
      <div className="tabbar">
        <div className="tabbar-content">
          {
            tarbarArr.map((v, i) => (
              <div key={i}
                className={"tabbar-item" + (this.state.index === i ? ' active' : '')}
                onClick={() => this.itemChange(v, i)}
              >
                <div style={{ lineHeight: "34px", height: "34px", width: "auto", content: "center" }} >
                  <img height="32px" src={v.img + (this.state.index === i ? '_select' : '') + ".png"} />
                </div>

                <div style={{ height: "16px", lineHeight: "16px", width: "auto", content: "center", fontSize: "9px" }}>{v.text}</div>
              </div>
            ))
          }
        </div>
      </div>
    );
  }
}

export default Tabbar;
