import {Spin} from 'antd';
import React from 'react';

export default () => (
  <div
    style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '100%' }}
  >
    <Spin size="large" />
  </div>
);
