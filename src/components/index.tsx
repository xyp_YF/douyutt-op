/**
 * 路由组件出口文件
 * yezi 2018年6月24日
 */
import Loadable from 'react-loadable';
import Loading from './widget/Loading';

import Tabbar from '././comm/mtab/Tabbar';


const H5LoginSuccess = Loadable({

  loader: () => import('./pages/h5/login/LoginSuccess'),
  loading: Loading,
});

const DouyuPage = Loadable({

  loader: () => import('./pages/h5/dy/DouyuPage'),
  loading: Loading,
});



const Dashboard = Loadable({

  loader: () => import('./dashboard/Dashboard'),
  loading: Loading,
});


const QiniuUploadImg = Loadable({

  loader: () => import('./comm/file/QiniuUploadImg'),
  loading: Loading,
});

const UploadImg = Loadable({

  loader: () => import('./comm/file/UploadImg'),
  loading: Loading,
});



// const UserFormIndex = Loadable({

//   loader: () => import('./pages/user/UserForm/UserFormIndex'),
//   loading: Loading,
// });


const UserPageIndex = Loadable({

  loader: () => import('./pages/user/UserPage/UserPageIndex'),
  loading: Loading,
});


const HeroAdmin = Loadable({

  loader: () => import('./pages/h5/hero/HeroAdmin'),
  loading: Loading,
});

const HeroFormIndex = Loadable({

  loader: () => import('./pages/h5/hero/HeroFormIndex'),
  loading: Loading,
});


const DouyuIndex = Loadable({

  loader: () => import('./pages/h5/dy/DouyuIndex'),
  loading: Loading,
});

const DouyuAdmin = Loadable({

  loader: () => import('./pages/h5/dy/DouyuAdmin'),
  loading: Loading,
});
const DouyuFormIndex = Loadable({

  loader: () => import('./pages/h5/dy/DouyuFormIndex'),
  loading: Loading,
});


//PageTabs

const H5Login = Loadable({

  loader: () => import('./pages/h5/login/Login'),
  loading: Loading,
});






export default {
  Dashboard,


  QiniuUploadImg,
  UploadImg,

  UserPageIndex,
  // UserFormIndex,

  H5Login,
  H5LoginSuccess,


  Tabbar,

  DouyuPage,
  DouyuAdmin,
  DouyuFormIndex,
  DouyuIndex,
  HeroFormIndex,
  HeroAdmin,
} as any;
