export interface IFMenuBase {
    key: string;
    title: string;
    icon?: string;
    component?: string;
    query?: string;
    requireAuth?: string;
    route?: string;
    /** 是否登录校验，true不进行校验（访客） */
    login?: boolean;
}

export interface IFMenu extends IFMenuBase {
    subs?: IFMenu[];
    children?: IFMenu[];
}

const menus: {
    menus: IFMenu[];
    others: IFMenu[] | [];
    [index: string]: any;
} = {
    menus: [
        // 菜单相关路由
        { key: '/app/index', title: '首页', icon: 'mobile', component: 'Dashboard',login: true },
        { key: '/app/dy/DouyuPage', title: '比赛情况', component: 'DouyuPage',login: true },
        { key: '/app/dy/DouyuAdmin', title: '排名管理', component: 'DouyuAdmin',login: true },
        { key: '/app/hero/HeroAdmin', title: '英雄列表', component: 'HeroAdmin',login: true },
    ],
    others: [
      
        { key: '/app/user/userForm', title: '用户表单', component: 'UserFormIndex',login: true },
        { key: '/app/test/Basic', title: '地图', component: 'RouterEnter',login: true },
        { key: '/app/dy/DouyuPage', title: '斗鱼', component: 'DouyuPage',login: true },
        { key: '/app/dy/DouyuAdmin', title: '斗鱼', component: 'DouyuAdmin',login: true },
        { key: '/app/dy/DouyuIndex', title: '斗鱼', component: 'DouyuIndex',login: true },
        { key: '/app/dy/DouyuFormIndex', title: '斗鱼', component: 'DouyuFormIndex',login: true },
        { key: '/app/hero/HeroAdmin', title: '斗鱼', component: 'HeroAdmin',login: true },
        { key: '/app/hero/HeroFormIndex', title: '斗鱼', component: 'HeroFormIndex',login: true },
 
    ],
};

export default menus;
