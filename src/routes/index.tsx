/**
 * Created by 叶子 on 2017/8/13.
 */
import React, { Component } from 'react';
import { Route, Redirect, Switch } from 'react-router-dom';
import DocumentTitle from 'react-document-title';
import AllComponents from '../components';
import routesConfig, { IFMenuBase, IFMenu } from './config';
import queryString from 'query-string';
import { checkLogin } from '../utils';
import { connectAlita } from 'redux-alita';
import umbrella from 'umbrella-storage';

type CRouterProps = {
  auth: any;
  smenus: any;
};

type CRouterState = {};

interface Permission {
  url: string;
  urlRoleKey: string;
  urlRoleId: number;
}

class CRouter extends Component<CRouterProps, CRouterState> {
  getPermits = (): any[] | null => {
    const permissions = ["oprationAdmin", "opration"];
    const { auth } = this.props;
    console.log("getPermits", auth);
    // TODO 根据后端获取. 目前是 只要登录了，都给默认的授权
    if (this.checkCurrentLogin(auth)) {
      return permissions;
    } else {
      return null;
    }
  };

  requireAuth = (permit: any, component: React.ReactElement) => {

    const permits = this.getPermits();
    // const { auth } = store.getState().httpData;
    if (!permits || !permits.includes(permit)) return <Redirect to={'404'} />;
    return component;
  };

  checkCurrentLogin = (params: any) => {

    return params && params.data && params.data.user && params.data.user.loginName;
  }

  requireLogin = (component: React.ReactElement, permit: any) => {
    // const permits = this.getPermits();
    // if (!checkLogin(permits)) {
    //   // 线上环境判断是否登录
    //   return <Redirect to={'/login'} />;
    // }

    // return permit ? this.requireAuth(permit, component) : component;

    const { auth } = this.props;
    if (!this.checkCurrentLogin(auth)) {
      return <Redirect to={'/login'} />;
    }
    //TODO 请求服务器获取权限并进行校验


    return component;
  };

  iterteMenu = (r: IFMenu) => {
    const route = (r: IFMenuBase) => {
      const Component = r.component && AllComponents[r.component];
      return (
        <Route
          key={r.route || r.key}
          exact
          path={r.route || r.key}
          render={props => {
            const reg = /\?\S*/g;
            // 匹配?及其以后字符串
            const queryParams = window.location.hash.match(reg);
            // 去除?的参数
            const { params } = props.match;
            Object.keys(params).forEach(key => {
              params[key] = params[key] && params[key].replace(reg, '');
            });
            props.match.params = { ...params };
            const merge = {
              ...props,
              query: queryParams ? queryString.parse(queryParams[0]) : {},
            };
            // 重新包装组件
            const wrappedComponent = (
              <DocumentTitle title={r.title}>
                <Component {...merge} />
              </DocumentTitle>
            );
            return r.login
              ? wrappedComponent
              : this.requireLogin(wrappedComponent, r.requireAuth);
          }}
        />
      );
    };

    const subRoute = (r: IFMenu): any =>
      r.subs && r.subs.map((subR: IFMenu) => (subR.subs ? subRoute(subR) : route(subR)));

    return r.component ? route(r) : subRoute(r);
  };

  createRoute = (key: string) => {
    return routesConfig[key].map(this.iterteMenu);
  };

  render() {
    const { smenus } = this.props;
    return (
      <Switch>
        {Object.keys(routesConfig).map(key => this.createRoute(key))}
        {(smenus.data || umbrella.getLocalStorage('smenus') || []).map(this.iterteMenu)}
        <Route render={() => <Redirect to="/404" />} />
      </Switch>
    );
  }
}

export default connectAlita([{ smenus: null }])(CRouter);
