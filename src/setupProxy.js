const proxy = require('http-proxy-middleware');
// 服务器环境下，不会执行代理
module.exports = function (app) {
    app.use(
        '/op',
        proxy.createProxyMiddleware({
            target: 'http://127.0.0.1:8080/',
            changeOrigin: true,
            pathRewrite: {
                '^/op': '',
            },
        })
    );
};
