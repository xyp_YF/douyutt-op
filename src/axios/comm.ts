/**
 * Created by hao.cheng on 2017/4/16.
 */
import axios from 'axios';
import { get, post, zbPost } from './tools';
import * as config from './config';

// export const fetchMenu = () => get({ url: config.MOCK_MENU });
export const fetchMenu = () => zbPost({ url: '/auth/menu/getMenuTree', data: {} });

export const login = (loginName: string, password: string) =>
    zbPost({ url: '/user/pwdLogin', data: { loginName, password }, msg: '' });

export const getCurrentUser = () => zbPost({ url: '/user/getCurrentLoginUser', data: {}, msg: '' });
